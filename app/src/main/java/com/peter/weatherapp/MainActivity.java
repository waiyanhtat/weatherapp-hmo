package com.peter.weatherapp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.androdocs.httprequest.HttpRequest;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener {

    String CITY;
    String API = "df89db26f16afcab8ad678f2aac166b1";
    TextView city,temperature,status,min,max,humidity,pressure;
    Double lon = 103.706442;
    Double lat = 1.318653;
    String byCity;
    String byLocation;
    private GoogleMap mMap;

    private static final int LOCATION_REQUEST_CODE = 101;
    private static final String TAG = "WeatherApp";
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final SearchView citySearch = findViewById(R.id.citySearch);
        city = findViewById(R.id.address);
        temperature = findViewById(R.id.temperature);
        status = findViewById(R.id.weatherStatus);
        min = findViewById(R.id.minTemp);
        max = findViewById(R.id.maxTemp);
        humidity = findViewById(R.id.humidity);
        pressure = findViewById(R.id.pressure);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        byCity = ("https://api.openweathermap.org/data/2.5/weather?q=singapore,sg" + "&units=metric&appid=" + API);
        new APICity().execute();


        //Search Bar
        citySearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                CharSequence search = citySearch.getQuery();
                CITY = String.valueOf(search);
                byCity = ("https://api.openweathermap.org/data/2.5/weather?q=" + CITY + "&units=metric&appid=" + API);
                new APICity().execute();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        //Check permission
        int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        if (permission != PackageManager.PERMISSION_GRANTED)
        {
            Log.i(TAG, "Permission to use location denied");
            makeRequest();
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        LocationRequest mLocationRequest;

        LocationCallback mLocationCallback;

        mLocationRequest = new LocationRequest();
        mLocationRequest .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                        .setInterval(10000L)
                        .setFastestInterval(5000L)
                        ;


        mLocationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                    LatLng latLng = new LatLng(locationResult.getLastLocation().getLatitude(),
                            locationResult.getLastLocation().getLongitude() );

                    requestMapByLocation(latLng);
            }
        };


        fusedLocationClient.requestLocationUpdates(mLocationRequest,mLocationCallback, Looper.myLooper());




    }

    class APICity extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Showing the ProgressBar, Making the main design GONE
            //findViewById(R.id.loader).setVisibility(View.VISIBLE);
            //findViewById(R.id.mainContainer).setVisibility(View.GONE);
            //findViewById(R.id.errorText).setVisibility(View.GONE);
        }

        protected String doInBackground(String... args) {
            String response = HttpRequest.excuteGet(byCity);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject jsonObj = new JSONObject(result);
                JSONObject coord = jsonObj.getJSONObject("coord");
                JSONObject main = jsonObj.getJSONObject("main");
                JSONObject sys = jsonObj.getJSONObject("sys");
                JSONObject weather = jsonObj.getJSONArray("weather").getJSONObject(0);

                Long updatedAt = jsonObj.getLong("dt");
                String country = jsonObj.getString("name") + ", " + sys.getString("country");
                String temp = main.getString("temp") + "°C";
                String weatherDescription = weather.getString("description");
                String tempMin = main.getString("temp_min") + "°C";
                String tempMax = main.getString("temp_max") + "°C";
                String humidityNo = main.getString("humidity") + "%";
                String pressurePsa = main.getString("pressure") + "hPa";
                String lastUpdated = "Updated at: " + new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH).format(new Date(updatedAt * 1000));


                city.setText(country);
                temperature.setText((temp));
                status.setText((weatherDescription));
                min.setText(tempMin);
                max.setText(tempMax);
                humidity.setText(humidityNo);
                pressure.setText(pressurePsa);

                /* Populating extracted data into our views
                addressTxt.setText(address);
                statusTxt.setText(weatherDescription.toUpperCase());


                Views populated, Hiding the loader, Showing the main design
                findViewById(R.id.loader).setVisibility(View.GONE);
                findViewById(R.id.mainContainer).setVisibility(View.VISIBLE);*/

                CITY = country;
                lon = coord.getDouble("lon");
                lat = coord.getDouble("lat");
                onMapReady(mMap);


            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(),"City not found. Please try again.",Toast.LENGTH_LONG).show();
            }

        }
    }

    class APILocation extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            Log.i(TAG, "preparing");
            super.onPreExecute();
            //Showing the ProgressBar, Making the main design GONE
            //findViewById(R.id.loader).setVisibility(View.VISIBLE);
            //findViewById(R.id.mainContainer).setVisibility(View.GONE);
            //findViewById(R.id.errorText).setVisibility(View.GONE);
        }

        protected String doInBackground(String... args) {
            Log.i(TAG, "fetching");
            String response = HttpRequest.excuteGet(byLocation);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.i(TAG, "processing");
            try {
                JSONObject jsonObj = new JSONObject(result);
                JSONObject coord = jsonObj.getJSONObject("coord");
                JSONObject main = jsonObj.getJSONObject("main");
                JSONObject sys = jsonObj.getJSONObject("sys");
                JSONObject weather = jsonObj.getJSONArray("weather").getJSONObject(0);

                Long updatedAt = jsonObj.getLong("dt");
                String lastUpdated = "Updated at: " + new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH).format(new Date(updatedAt * 1000));

                String temp = main.getString("temp") + "°C";
                String tempMin = "Min Temp: " + main.getString("temp_min") + "°C";
                String tempMax = "Max Temp: " + main.getString("temp_max") + "°C";

                String weatherDescription = weather.getString("description");
                String country = jsonObj.getString("name") + ", " + sys.getString("country");

                lon = coord.getDouble("lon");
                lat = coord.getDouble("lat");
                CITY = country;
                city.setText(country);
                temperature.setText(String.valueOf(temp));
                status.setText(String.valueOf(weatherDescription));
                /* Populating extracted data into our views
                addressTxt.setText(address);
                statusTxt.setText(weatherDescription.toUpperCase());
                tempTxt.setText(temp);
                temp_minTxt.setText(tempMin);
                temp_maxTxt.setText(tempMax);

                Views populated, Hiding the loader, Showing the main design
                findViewById(R.id.loader).setVisibility(View.GONE);
                findViewById(R.id.mainContainer).setVisibility(View.VISIBLE);*/
                onMapReady(mMap);


            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(),"Sorry, there is no weather information for this location. Please try another location.",Toast.LENGTH_LONG).show();
                Log.i(TAG, "Error" + result);
            }

        }
    }


    protected void makeRequest()
    {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                LOCATION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[]grantResults)
    {
        switch (requestCode)
        {
            case LOCATION_REQUEST_CODE:
            {
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED)
                {
                    Log.i(TAG, "Permission has been denied by user");
                }
                else
                {
                    Log.i(TAG, "Permission has been granted by user");
                }
                return;
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.setOnMapClickListener(this);

        try {
            // Customise the styling of the base map using a JSON object defined
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));
            if (!success) {
            }
        } catch (Resources.NotFoundException e) {
        }

        LatLng location = new LatLng(lat, lon);

        CameraPosition oldCameraPosition = mMap.getCameraPosition();
        Log.i(TAG, Double.toString(oldCameraPosition.zoom));

        mMap.clear();
        MarkerOptions marker = new MarkerOptions()
                .position(location)
                .title(CITY)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .alpha(0.8f);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(location)
                .zoom(oldCameraPosition.zoom)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        googleMap.addMarker(marker);

        UiSettings mapUiSettings = mMap.getUiSettings();
        mapUiSettings.setZoomGesturesEnabled(true);
        mapUiSettings.setMapToolbarEnabled(false);
        mapUiSettings.setZoomControlsEnabled(true);
        mapUiSettings.setMyLocationButtonEnabled(true);

    }

    //Create  map marker with a tap
    @Override
    public void onMapClick(LatLng latLng) {
        Log.i(TAG, "Latitude is " + Double.toString(latLng.latitude));
        Log.i(TAG, "Longitude is " + Double.toString(latLng.longitude));
        lon = latLng.longitude;
        lat = latLng.latitude;
        byLocation = ("https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&units=metric&appid=" + API);
        new APILocation().execute();
    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    private void requestMapByLocation(LatLng latLng){
        lon = latLng.longitude;
        lat = latLng.latitude;
        byLocation = ("https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&units=metric&appid=" + API);
        new APILocation().execute();
    }
}
